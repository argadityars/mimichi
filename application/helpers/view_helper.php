<?php

use Jenssegers\Blade\Blade;

if (!function_exists('view'))
{
    function view($view, $data = [])
    {
        $path = APPPATH . 'views';
        $cache = APPPATH . 'cache';
        $blade = new Blade($path, $cache);

        echo $blade->make($view, $data);
    }
}