<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pages extends CI_Controller
{
    public function _remap($method)
    {
        is_file(APPPATH . 'views/pages/' . $method . '.blade.php') or show_404();

        return view("pages.$method");
    }
}
